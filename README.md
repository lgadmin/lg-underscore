# README #

### What is this repository for? ###

* Starter Theme for Longevity Graphics 
* Version 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repository into the project theme folder.
* npm install
* gulp getassets
* gulp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jason Stone, Kelvin Xu, Stefan Adam
* 