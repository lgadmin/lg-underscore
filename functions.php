<?php
	function lg_enqueue_styles_scripts() {
	    
	    // Child theme style sheet setup.
	    $parent_style = '_s-style';
	    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );

		// Fonts
		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i', false );

		// Register Scripts
		wp_register_script( 'bootstrapjs', get_stylesheet_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.7', true );
		wp_register_script( 'lg-scripts', get_stylesheet_directory_uri() . "/js/lg-scripts.js", array('jquery'), false, true );
 
		// Enqueue Scripts.
		wp_enqueue_script( 'bootstrapjs' );
		wp_enqueue_script( 'lg-ga' );
	}
	add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


	//Dequeue JavaScripts
	function project_dequeue_unnecessary_scripts() {
		wp_dequeue_script( '_s-navigation' );
		wp_deregister_script( '_s-navigation' );
	}
	add_action( 'wp_print_scripts', 'project_dequeue_unnecessary_scripts' );

	// Format Phone Numbers Function
	function format_phone($phone)
	{
		$phone = preg_replace("/[^0-9]/", "", $phone);
	 
		if(strlen($phone) == 7)
			return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
		elseif(strlen($phone) == 10)
			return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
		else
			return $phone;
	}
?>